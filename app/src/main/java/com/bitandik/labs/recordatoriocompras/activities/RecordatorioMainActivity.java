package com.bitandik.labs.recordatoriocompras.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.bitandik.labs.recordatoriocompras.R;
import com.bitandik.labs.recordatoriocompras.RecordatorioApplication;
import com.bitandik.labs.recordatoriocompras.adapters.RecordatorioItemsRecyclerAdapter;
import com.bitandik.labs.recordatoriocompras.models.ToBuyItem;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecordatorioMainActivity extends AppCompatActivity {
  @BindView(R.id.recycler_view_items) RecyclerView recyclerView;
  @BindView(R.id.editTextItem) EditText editTextItem;

  private DatabaseReference databaseReference;
  private FirebaseRecyclerAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    setupUsername();
    SharedPreferences prefs = getApplication().getSharedPreferences("ToDoPrefs", 0);
    String username = prefs.getString("username", null);
    setTitle("items para comprar: " + username);

    RecordatorioApplication app = (RecordatorioApplication)getApplicationContext();
    databaseReference = app.getItemsReference();
    adapter = new RecordatorioItemsRecyclerAdapter(R.layout.row, databaseReference);

    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(adapter);
  }

  private void setupUsername() {
    SharedPreferences prefs = getApplication().getSharedPreferences("ToDoPrefs", 0);
    String username = prefs.getString("username", null);
    if (username == null) {
        Random r = new Random();
        username = "usuario" + r.nextInt(100000);
        prefs.edit().putString("username", username).commit();
    }
  }

  @OnClick(R.id.fab)
  public void addToDoItem() {
    SharedPreferences prefs = getApplication().getSharedPreferences("ToDoPrefs", 0);
    String username = prefs.getString("username", null);

    String itemText = editTextItem.getText().toString();
    editTextItem.setText("");

    InputMethodManager inputMethodManager = (InputMethodManager)  getSystemService(Activity.INPUT_METHOD_SERVICE);
    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

    if (!itemText.isEmpty()) {
        ToBuyItem toBuyItem = new ToBuyItem(itemText.trim(), username);
        databaseReference.push().setValue(toBuyItem);
    }
  }
}