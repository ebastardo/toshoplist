package com.bitandik.labs.recordatoriocompras.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitandik.labs.recordatoriocompras.R;
import com.bitandik.labs.recordatoriocompras.models.ToBuyItem;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecordatorioItemsRecyclerAdapter extends FirebaseRecyclerAdapter<ToBuyItem,
        RecordatorioItemsRecyclerAdapter.ToBuyItemViewHolder> {

  public RecordatorioItemsRecyclerAdapter(int modelLayout, DatabaseReference ref) {
    super(ToBuyItem.class, modelLayout, ToBuyItemViewHolder.class, ref);
  }

  @Override
  public ToBuyItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ViewGroup view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(mModelLayout, parent, false);
    return new ToBuyItemViewHolder(view);
  }

  @Override
  protected void populateViewHolder(ToBuyItemViewHolder holder, ToBuyItem item, int position) {
    String itemDescription = item.getItem();
    String username = item.getUsername();

    holder.txtItem.setText(itemDescription);

    holder.txtUser.setText("Modified:" + new Date().toString());

    if (item.isCompleted()) {
      holder.imgDone.setVisibility(View.VISIBLE);
    } else {
      holder.imgDone.setVisibility(View.INVISIBLE);
    }
  }

  class ToBuyItemViewHolder extends RecyclerView.ViewHolder
                           implements View.OnClickListener,
                                      View.OnLongClickListener {

    @BindView(R.id.txtItem) TextView txtItem;
    @BindView(R.id.txtUser) TextView txtUser;
    @BindView(R.id.imgDone) ImageView imgDone;

    public ToBuyItemViewHolder(View itemView) {
      super(itemView);
      itemView.setOnClickListener(this);
      itemView.setOnLongClickListener(this);
      ButterKnife.bind(this, itemView);
    }

    @Override
    public void onClick(View view) {
      int position = getAdapterPosition();
      ToBuyItem currentItem = (ToBuyItem)getItem(position);
      DatabaseReference reference = getRef(position);
      boolean completed = !currentItem.isCompleted();

      currentItem.setCompleted(completed);
      Map<String, Object> updates = new HashMap<String, Object>();
      updates.put("completed", completed);
      reference.updateChildren(updates);
    }

    @Override
    public boolean onLongClick(View view) {
      int position = getAdapterPosition();
      DatabaseReference reference = getRef(position);
      reference.removeValue();
      return true;
    }
  }
}

